sudo sed -i '1s/^/nameserver 10.49.10.21\n/' /etc/resolv.conf


docker network create --subnet=172.20.0.0/16 fortinet
sudo ip route add 10.46.0.0/16 via 172.20.0.2
sudo ip route add 10.49.0.0/16 via 172.20.0.2
sudo ip route add 10.42.0.0/16 via 172.20.0.2
sudo ip route add 10.47.0.0/16 via 172.20.0.2 #fifthplay net
#useful @fifthplay france offices so we can go through the closed ports on outgoing
DOMBOX_NET=$(sudo ifconfig | grep 192.168.0)
if [ -n "$DOMBOX_NET" ]; then
	TESTBUILDSRV=$(host testbuild.cloudapp.net | awk '{print $4}')
	echo -ne "Inside Dombox network\n"
	sudo ip route add $TESTBUILDSRV via 172.20.0.2
fi

docker  run -it --rm --privileged --net fortinet --ip 172.20.0.2 -e VPNTIMEOUT=30 -e VPNADDR=81.83.24.121:443 -e VPNUSER=XXX -e VPNPASS=YYYY jorgesr86/forticlient_ubuntu

sudo ip route del 10.46.0.0/16 via 172.20.0.2
sudo ip route del 10.49.0.0/16 via 172.20.0.2
sudo ip route del 10.42.0.0/16 via 172.20.0.2
sudo ip route del 10.47.0.0/16 via 172.20.0.2
if [ -n "$DOMBOX_NET" ]; then
	sudo ip route del $TESTBUILDSRV via 172.20.0.2
fi

docker network rm fortinet

sudo sed '1d'  -i /etc/resolv.conf

